DROP TABLE IF EXISTS items;

CREATE TABLE items ( 
   id UUID NOT NULL, 
   name VARCHAR(50) NOT NULL, 
   price  DOUBLE NOT NULL,
   unit_price DOUBLE,
   unit INT NOT NULL
);

INSERT INTO items (id,name,price,unit_price,unit)VALUES 
('083b3090-0f8f-47a4-a138-8bff452cae7f', 'Penguine Ear', 175,8.75, 20),
('96ed0e5e-9b12-4a10-83d2-4976e1fc5ee1', 'Horseshoe', 825,165, 5); 
