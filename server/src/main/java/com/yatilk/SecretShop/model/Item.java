/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yatilk.SecretShop.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author yatilk
 */
@Entity
@Table(name = "items")
public class Item {

    @Id
    private UUID id;
    private String name;
    private double price;
    private double unitPrice;
    private int unit;

//    public Item(@JsonProperty("name") String name, @JsonProperty("price") double price, @JsonProperty("unit") int unit, @JsonProperty("id") String id) {
//        this.name = name;
//        this.price = price;
//        this.unit = unit;
//        this.id = id;
//    }
//
//    public Item() {
//    }
    public String getName() {
        return name;
    }

    public UUID getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public int getUnit() {
        return unit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }
}
