/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yatilk.SecretShop.api;

import com.yatilk.SecretShop.model.Item;
import com.yatilk.SecretShop.service.ItemService;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author yatilk
 */
@RequestMapping("/api/v1/items")
@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class ItemController {

    private final ItemService itemService;

    @Autowired
    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping
    public @ResponseBody
    Iterable<Item> getItems() {
        return itemService.getItems();
    }

    @GetMapping("/list/{id}")
    public @ResponseBody
    Iterable<Item> getItemList(@PathVariable("id") UUID id) {
        return itemService.getItemList(id);
    }

    @GetMapping("/total/{id}/{units}")
    public @ResponseBody
    double calculateTotal(@PathVariable("id") UUID productId, @PathVariable("units") int units) {
        return itemService.calculateTotal(productId, units);
    }

}
