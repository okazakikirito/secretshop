/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yatilk.SecretShop.repositary;

import com.yatilk.SecretShop.model.Item;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author yatilk
 */
@Repository("db")
public interface ItemRepositary extends CrudRepository<Item, UUID> {

}
