/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yatilk.SecretShop.service;

import com.yatilk.SecretShop.model.Item;
import com.yatilk.SecretShop.repositary.ItemRepositary;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author yatilk
 */
@Service
public class ItemService {

    private final ItemRepositary itemRepositary;
//    private Iterable<Item> itemList;
    Map<String, Object> itemMap = new HashMap<>();
    private List itemList = new ArrayList();
    private static final double LABOUR_COST = 1.3;
    private static final double SPECIAL_DISCOUNT = 0.9;

    @Autowired
    public ItemService(@Qualifier("db") ItemRepositary itemRepositary) {
        this.itemRepositary = itemRepositary;
    }

    public Iterable<Item> getItems() {
        return itemRepositary.findAll();
    }

    public Optional<Item> getItemById(UUID id) {
        return itemRepositary.findById(id);
    }

    public Iterable<Item> getItemList(UUID id) {
//    public List getItemList() {
        Item item = getItemById(id).get();
        UUID itemId = item.getId();
        double unitPrice = item.getUnitPrice();
        for (int i = 1; i <= 50; i++) {
            Item newItem = new Item();
            newItem.setUnitPrice(calculateUnitPrice(unitPrice, i));
            newItem.setId(itemId);
            newItem.setUnit(i);
            newItem.setName(item.getName());
            itemList.add(newItem);
        }
        return itemList;
    }

    public double calculateUnitPrice(double price, int units) {
        return units * price * LABOUR_COST;
    }

    public double calculateTotal(UUID id, int unitsRequested) {
        try {
            return optimizeOrder(id, unitsRequested);
        } catch (Exception ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    private double optimizeOrder(UUID id, int unitsRequested) throws Exception {
        Item item = itemRepositary.findById(id).orElse(null);
        if (item != null) {
            int unitsPerCarton = item.getUnit();
            int cartons = unitsRequested / unitsPerCarton;
            int units = unitsRequested % unitsPerCarton;
            return specialPrice(item, cartons, units);
        } else {
            throw new Exception("Visit the super-secret shop for more items");
        }

    }

    private double specialPrice(Item item, int cartons, int units) {
        double totalPrice = 0.0;
        double cartonPrice = item.getPrice();
        double pricePerUnit = item.getUnitPrice();
        double unitPrice = calculateUnitPrice(pricePerUnit, units);

        if (cartons >= 3) {
            double discountCartonPrice = cartonPrice * SPECIAL_DISCOUNT;
            totalPrice = (discountCartonPrice * cartons);
        } else if (cartons <= 1) {
            totalPrice = (cartonPrice * cartons);
        }
        totalPrice += unitPrice;
        return totalPrice;
    }
}
