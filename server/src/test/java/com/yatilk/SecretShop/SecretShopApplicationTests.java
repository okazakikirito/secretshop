package com.yatilk.SecretShop;

import com.yatilk.SecretShop.api.ItemController;
import com.yatilk.SecretShop.model.Item;
import java.util.List;
import java.util.UUID;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SecretShopApplicationTests {
    
    
    @Autowired
    private ItemController controller;
    private UUID pengUUID = UUID.randomUUID();
    private UUID horseUUID = UUID.randomUUID();
    private UUID kPengUUID = UUID.fromString("083b3090-0f8f-47a4-a138-8bff452cae7f");
    private UUID kHorseUUID = UUID.fromString("96ed0e5e-9b12-4a10-83d2-4976e1fc5ee1");
    @Test
    public void getList() {

        // compare two products
        Item expectedPenguine = new Item();
        expectedPenguine.setId(kPengUUID);
        expectedPenguine.setName("Penguine Ear");
        expectedPenguine.setUnit(20);
        expectedPenguine.setUnitPrice(8.75);
        expectedPenguine.setPrice(175.0);

        Item expectedHorseShoe = new Item();
        expectedHorseShoe.setId(kHorseUUID);
        expectedHorseShoe.setName("Horseshoe");
        expectedHorseShoe.setUnit(5);
        expectedHorseShoe.setUnitPrice(165);
        expectedHorseShoe.setPrice(825.0);

        List<Item> itemList = (List<Item>) controller.getItems();
        Item actualPenguine = itemList.get(0);
        Item actualHorseShoe = itemList.get(1);

        assertThat(actualPenguine).isEqualToIgnoringGivenFields(expectedPenguine);
        assertThat(actualHorseShoe).isEqualToIgnoringGivenFields(expectedHorseShoe);

        // compare lists
    }

    /**
     * price testing for product penguine ears
     */
    @Test
    public void getPenguinePrices() {
        double actualPrice = controller.calculateTotal(kPengUUID, 7);
        assertThat(actualPrice).isEqualTo(79.625);

        actualPrice = controller.calculateTotal(kPengUUID, 25);
        assertThat(actualPrice).isEqualTo(231.875);

        actualPrice = controller.calculateTotal(kPengUUID, 68);
        assertThat(actualPrice).isEqualTo(563.5);
    }

    /**
     * price testing for horse shoes
     */
    @Test
    public void getHorsePrices() {
        double actualPrice = controller.calculateTotal(kHorseUUID, 3);
        assertThat(actualPrice).isEqualTo(643.5);

        actualPrice = controller.calculateTotal(kHorseUUID, 11);
        assertThat(actualPrice).isEqualTo(214.5);

        actualPrice = controller.calculateTotal(kHorseUUID, 47);
        assertThat(actualPrice).isEqualTo(7111.5);
    }
}
