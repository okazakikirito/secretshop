import React, { Component } from "react";
import { Link } from "react-router-dom";
import AppNavbar from "./AppNavbar";
import { Tabs, Tab, Table } from "react-bootstrap";

class ItemTable extends Component {
  state = {
    isLoading: true,
    items: [],
    newItems: [],
  };

  async componentDidMount() {
    console.log("component did mount");
    const response = await fetch(
      "/items/list/083b3090-0f8f-47a4-a138-8bff452cae7f"
    );
    const body = await response.json();

    const response2 = await fetch(
      "/items/list/96ed0e5e-9b12-4a10-83d2-4976e1fc5ee1"
    );
    const body2 = await response2.json();

    this.setState({ items: body, newItems: body2, isLoading: false });
  }
  renderPenList() {
    return this.state.items.map((item, i) => {
      return (
        <tr key={item.unit}>
          <td>{item.name}</td>
          <td>{item.unit}</td>
          <td>{item.unitPrice}</td>
        </tr>
      );
    });
  }
  render() {
    const { items, newItems, isLoading } = this.state;
    if (isLoading) {
      return <p>Wait Gnomes Are Collecting Item Data!!</p>;
    }
    return (
      <div>
        <AppNavbar />
        <Tabs defaultActiveKey="PenguineEar" id="uncontrolled-tab-example">
          <Tab eventKey="PenguineEar" title="Penguine Ears Price List">
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>Item Name</th>
                  <th>Units</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                {items.map((Item,i) => (
                  <tr key={Item.id}>
                    <td>{Item.name}</td>
                    <td>{Item.unit}</td>
                    <td>{Item.unitPrice}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Tab>
          <Tab eventKey="horseshoe" title="Horseshoes Price List">
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>Item Name</th>
                  <th>Units</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                {newItems.map((Item,i) => (
                  <tr key={Item.id}>
                    <td>{Item.name}</td>
                    <td>{Item.unit}</td>
                    <td>{Item.unitPrice}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Tab>
        </Tabs>
      </div>
    );
  }
}

export default ItemTable;
