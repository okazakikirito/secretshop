import React, { Component } from "react";
// import {
//   Collapse,
//   Nav,
//   Navbar,
//   NavbarBrand,
//   NavbarToggler,
//   NavItem,
//   NavLink,
// } from "reactstrap";
import {Nav} from 'react-bootstrap'
import { Link } from "react-router-dom";

export default class AppNavbar extends Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  render() {
    return (
      <Nav justify variant="tabs" defaultActiveKey="/">
        <Nav.Item>
          <Nav.Link href="/">Home</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link href="/items" eventKey="link-1">Item Shop</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="disabled" disabled>
            Secret Shop
          </Nav.Link>
        </Nav.Item>
      </Nav>
    );
  }
}
