import React, { Component, useState } from "react";
import peng from "./peng.jpg";
import horseShoe from "./horseShoe.jpg";
import { Link } from "react-router-dom";
import AppNavbar from "./AppNavbar";
import "./items.css";
import { Card, Button, CardDeck, CardGroup } from "react-bootstrap";
class Items extends Component {
  state = {
    isLoading: true,
    items: [],
    total: 0,
    pengUnits: 0,
    horseUnits: 0,
    pengCartons: 0,
    horseCartons: 0,
    pengaf: 0,
    horseaf: 0,
    value: 0,
  };

  async componentDidMount() {
    const response = await fetch("/items");
    const body = await response.json();
    this.setState({ items: body, isLoading: false });
  }

  renderDetails(name) {
    if (name === "Penguine Ear")
      return "This is a super rare item that can only be found in our shop";
    return "Not as rare as Penguines Ear but stil hard to find";
  }

  renderImage(name) {
    if (name === "Penguine Ear")
      return (
        <Card.Img
          variant="top"
          src="https://media.istockphoto.com/vectors/penguin-icon-vector-id926934398?b=1&k=6&m=926934398&s=612x612&w=0&h=b5v15hOxKA5_qfWFss70wXZBNg03ZRExuOG514jDJ1c=/100px180"
        />
      );
    return <Card.Img variant="top" src="{horseShoe}/100px180" />;
  }


  async calculateTotal() {
    console.log("calculating the total");

    const pengTot = await fetch("items/total/083b3090-0f8f-47a4-a138-8bff452cae7f/"+this.state.pengaf);
    const pengBody = await pengTot.json();

    
    const horseTot = await fetch("items/total/96ed0e5e-9b12-4a10-83d2-4976e1fc5ee1/"+this.state.horseaf);
    const horseBody = await horseTot.json();

    const total = pengBody + horseBody;
    this.setState({total : total});
    
  }
constructor(){

    super();
    this.setQuantity = this.setQuantity.bind(this);
    this.calculateTotal = this.calculateTotal.bind(this);
}
async setQuantity (name, e) {
    if (e.target.value != isNaN()){
      console.log(e.target.value)
    name === "Penguine Ear" ? await this.setState({pengaf: e.target.value}) :await this.setState({horseaf:e.target.value});
    this.calculateTotal() 
    }
  };


  render() {
    const { items, isLoading } = this.state;

    if (isLoading) {
      return <p>Wait Gnomes Are Collecting Items!!</p>;
    }
    return (
      <div>
        <AppNavbar />
        <div className="items">
          {items.map((Item) => (
            <div className="alignment:center m-3">
              <CardGroup>
                <Card style={{ width: "4rem" }}>
                  {this.renderImage}
                  <Card.Body>
                    <Card.Title>{Item.name}</Card.Title>
                    <Card.Text>{this.renderDetails(Item.name)}</Card.Text>
                    <label>
                      Quantity
                      <input
                        type="int"
                        value={Item.name === "Penguine Ear" ? this.state.pengaf : this.state.horseaf}
                        onChange={(e) =>
                          this.setQuantity(Item.name, e)
                        }
                      />
                    </label>
                    {this.renderUnits}
                  </Card.Body>
                </Card>
              </CardGroup>
            </div>
          ))}

        </div>
        <div className="text-center">
          <h3>Total:{this.state.total} </h3>
          {/* <Button className="btn-primary">Confirm Order</Button> */}
        </div>
      </div>
    );
  }
}

export default Items;
