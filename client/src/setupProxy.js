const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/shop',
    createProxyMiddleware({
      target: 'http://localhost:8080/shop/api/v1',
      changeOrigin: true,
    })
  );
};