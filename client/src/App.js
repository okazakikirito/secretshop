import React, { Component } from 'react';
import './App.css';
// import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
// import GroupList from './GroupList';
import Items from './components/Items';
import ItemTable from './components/ItemTable';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/' exact={true} component={ItemTable}/>
          <Route path='/items' exact={true} component={Items}/>
        </Switch>
      </Router>
    )
  }
}

export default App;