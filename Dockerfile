FROM node:13.12.0-alpine

ENV WILDFLY_VERSION 21.0.0.Final
ENV WILDFLY_SHA1 0697db2640e87ef1073a374a67d1c14560f6bf74
ENV JBOSS_HOME /opt/jboss/wildfly/wildfly-21.0.0.Final
ENV HOME /opt/jboss

RUN mkdir -p /opt/jboss
RUN mkdir -p /opt/jboss/wildfly


RUN apk update && apk add tar openssl bash curl jq gcc libc-dev util-linux fortune sed findutils openssh gettext git maven apache-ant \
      && apk --no-cache add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community

RUN cd $HOME \
&& curl -O https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz \
&& sha1sum wildfly-$WILDFLY_VERSION.tar.gz | grep $WILDFLY_SHA1 \
&& tar xf wildfly-$WILDFLY_VERSION.tar.gz \
&& mv $HOME/wildfly-$WILDFLY_VERSION $JBOSS_HOME \
&& rm wildfly-$WILDFLY_VERSION.tar.gz 
# && chown -R jboss:0 ${JBOSS_HOME} \
# && chmod -R g+rw ${JBOSS_HOME}

# Ensure signals are forwarded to the JVM process correctly for graceful shutdown
ENV LAUNCH_JBOSS_IN_BACKGROUND true
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk
ENV PATH $JAVA_HOME/bin:$PATH

RUN mkdir /client
COPY /server/deployments/SecretShop-0.0.1-SNAPSHOT.war /opt/jboss/wildfly/wildfly-21.0.0.Final/standalone/deployments/

WORKDIR /client

ENV PATH /client/node_modules/.bin:$PATH

COPY client/package.json ./
COPY client/package-lock.json ./
RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent
# RUN npm start
# add app
COPY client ./
COPY /server/startupScripts/ /client
EXPOSE 8080 3000

CMD ["/client/start.sh"]