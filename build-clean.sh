#!/bin/bash

 VERSION=${1:-latest}

echo "Building Secret Shop Services"
cd server
mvn clean install 


echo "Building Secret Shop Image"

docker build -t secret-shop/demo:$VERSION .