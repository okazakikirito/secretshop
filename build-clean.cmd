@echo off

setlocal

set VERSION=%1
if [%VERSION%] == [] set VERSION=latest

echo "Building Secret Shop Services"
cd server
call mvn clean install 


echo "Building Secret Shop Image"

cd ..
docker build -t secret-shop/demo:%VERSION% .